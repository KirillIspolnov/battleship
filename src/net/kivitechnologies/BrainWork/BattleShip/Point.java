package net.kivitechnologies.BrainWork.BattleShip;

public class Point {
    /**
     * Константы, описывающие содержимое, распологающееся в точке
     * EMPTY       - пусто
     * WHOLE_SHIP  - целый сегмент корабля
     * BROKEN_SHIP - разрушенный сегмент корабля
     */
    public static final int EMPTY = 0x2,
                            WHOLE_SHIP = 0x4,
                            BROKEN_SHIP = 0x8;
         
    /**
     * Алфавит координатной плоскости
     */
    public static final String ALPHABET = "АБВГДЕЖЗИК";
    
    /**
     * Возвращает массив координат, полученный из координат вида А1
     * певрая координата - по оси абсции, вторая - по оси ординат
     * 
     * @param coordinate строковое представление координаты 
     * @return массив координат
     */
    public static int[] parsePosition(String coordinate) {
        return new int[] {
                ALPHABET.indexOf(coordinate.charAt(0)),
                Integer.parseInt(coordinate.substring(1, coordinate.length()))
        };
    }
    
    /**
     * координаты точки
     */
    private int x, y;
    /**
     * содержимое точки
     */
    private int content;
    
    public Point(String coordinate, int content) {
        int[] coordinates = parsePosition(coordinate);
        x = coordinates[0];
        y = coordinates[1];
        this.content = content;
    }
    
    public Point(String coordinate) {
        this(coordinate, EMPTY);
    }
    
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public Point nextHorizontal(int step) {
        return new Point(x + step, y);
    }
    
    public Point nextVertical(int step) {
        return new Point(x, y + step);
    }
    
    public Point lastHorizontal(int step) {
        return nextHorizontal(-step);
    }
    
    public Point lastVertical(int step) {
        return nextVertical(-step);
    }
    
    public int getContent() {
        return content;
    }
    
    public void setContent(int content) {
        this.content = content;
    }
    
    public String toString() {
        return String.format("%s%d", ALPHABET.charAt(x), y);
    }
}
