package net.kivitechnologies.BrainWork.BattleShip;

public enum Direction {
    HORIZONTAL, VERTICAL
}
