package net.kivitechnologies.BrainWork.BattleShip;

import java.util.ArrayList;

public class Field {
    private ArrayList<Point> filledPoints;
    
    public Field() {
        
    }
    
    public void addShip(Ship ship) throws IncorrectPosition {
        ArrayList<Point> shipPoint = ship.getAllPoints();
        filledPoints.addAll(ship.getAllPoints());
    }
    
    public static class IncorrectPosition extends Exception {
        private static final long serialVersionUID = 750725464380264926L;

        @Override
        public String toString() {
            return "";
        }
    }
}
