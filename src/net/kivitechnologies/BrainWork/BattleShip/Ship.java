package net.kivitechnologies.BrainWork.BattleShip;

import java.util.ArrayList;

public class Ship {
    /**
     * Строка форматирования для метода toString
     */
    private static final String OBJECT_FORMAT = "Корабль, стоящий по координатам %s цел на %d/%d";

    /**
     * Размер корабля
     */
    public int size;
    
    /**
     * Коллекция координат точек корабля, в которые пользователь не попадал 
     */
    private ArrayList<Point> coordinates;
    
    /**
     * Конструктор
     * Создает новый корабль, нос которого располагается в точке с координатой start,
     * а другое части - в зависимости от направления корабля
     * 
     * @param start координата носа корабля
     */
    public Ship(Point start, int size, Direction dir) {
        this.coordinates = new ArrayList<Point>();
        this.size = size;
        
        coordinates.add(start);
        for(int i = 1; i < size; i++) {
            if(dir == Direction.HORIZONTAL)
                coordinates.add(start.nextHorizontal(i));
            else
                coordinates.add(start.nextVertical(i));
        }
    }
    
    /**
     * Возвращает true, если корабль полностью цел, false в любом другом случае 
     * 
     * @return true, если корабль полностью цел, false в любом другом случае
     */
    public boolean isAlive() {
        return !coordinates.isEmpty();
    }
    
    /**
     * Если в точке с координатой x находится одна из частей корабля, 
     * то эта часть будет атакована и удалена из коллекции coordinates 
     * Метод возвращает true, если в точке с координатой x находится одна из частей корабля, 
     * false в любом другом случае
     * 
     * @param x координата атакуемой точки
     * @return true, если в точке с координатой x находится одна из частей корабля, false в любом другом случае
     */
    public boolean attack(int x) {
        int indexOfCoordinate = coordinates.indexOf(x);
        if(indexOfCoordinate == -1)
            return false;
        
        coordinates.remove(indexOfCoordinate);
        return true;
    }
    
    /**
     * Возвращает количество целый частей корабля
     *  
     * @return количество целый частей корабля
     */
    public int getWholePartsCount() {
        return coordinates.size();
    }
    
    /**
     * Возвращает коллекцию сегментов корабля
     * 
     * @return
     */
    public ArrayList<Point> getAllPoints() {
        return coordinates;
    }
    
    @Override
    public String toString() {
        return String.format(OBJECT_FORMAT, coordinates, coordinates.size(), size);
    }
}
