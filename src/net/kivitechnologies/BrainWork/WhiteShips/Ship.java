package net.kivitechnologies.BrainWork.WhiteShips;

import java.util.ArrayList;

public class Ship {
    /**
     * Строка форматирования для метода toString
     */
    private static final String OBJECT_FORMAT = "Корабль, стоящий в клетках %s цел на %d/%d";

    /**
     * Размер корабля в нашей "игре"
     */
    public static final int SIZE = 3;
    
    /**
     * Коллекция координат точек корабля, в которые пользователь не попадал 
     */
    private ArrayList<Integer> coordinates;
    
    /**
     * Конструктор
     * Создает новый корабль, нос которого располагается в точке с координатой start,
     * а другое части - в точках start + 1 и start + 2
     * 
     * @param start координата носа корабля
     */
    public Ship(int start) {
        coordinates = new ArrayList<Integer>();
        
        for(int i = 0; i < SIZE; i++) {
            coordinates.add(start + i);
        }
    }
    
    /**
     * Возвращает true, если корабль полностью цел, false в любом другом случае 
     * 
     * @return true, если корабль полностью цел, false в любом другом случае
     */
    public boolean isAlive() {
        return !coordinates.isEmpty();
    }
    
    /**
     * Если в точке с координатой x находится одна из частей корабля, 
     * то эта часть будет атакована и удалена из коллекции coordinates 
     * Метод возвращает true, если в точке с координатой x находится одна из частей корабля, 
     * false в любом другом случае
     * 
     * @param x координата атакуемой точки
     * @return true, если в точке с координатой x находится одна из частей корабля, false в любом другом случае
     */
    public boolean attack(int x) {
        int indexOfCoordinate = coordinates.indexOf(x);
        if(indexOfCoordinate == -1)
            return false;
        
        coordinates.remove(indexOfCoordinate);
        return true;
    }
    
    /**
     * Возвращает количество целый частей корабля
     *  
     * @return количество целый частей корабля
     */
    public int getWholePartsCount() {
        return coordinates.size();
    }
    
    @Override
    public String toString() {
        return String.format(OBJECT_FORMAT, coordinates, coordinates.size(), SIZE);
    }
}
