package net.kivitechnologies.BrainWork.WhiteShips;

import java.util.Scanner;

public class Game {
    private static final String INCORRECT_COORDINATE_FORMAT = "Кажется вы допустили ошибку (координата должна быть больше 0 и меньше %d), попробуйте еще: ",
                                ENTER_COORDINATE_MESSAGE = "Введите координату точки, по которой нужно открыть огонь: ",
                                RESULT_EGOR_MESSAGE = "С трудом потопил... Вам стоит потренироваться!",
                                RESULT_GOOD_MESSAGE = "Убил! Неплохо!",
                                RESULT_PIRATE_MESSAGE = "Убил! Ух, ну Вы и пират!";

    /**
     * Длина канала
     */
    public static final int LENGTH_OF_CHANNEL = 15;
    
    /**
     * Корабль
     */
    private Ship ship;
    /**
     * Сканнер для ввода координат
     */
    private Scanner scanner;
    /**
     * Количество атак
     */
    private int countOfAttacks;
    
    /**
     * Конструктор
     * Создает игру, готовую к запуску - создает корабль в случайной точке
     * и сканнер для ввода данных 
     */
    public Game() {
        ship = new Ship((int)(Math.random() * (LENGTH_OF_CHANNEL - Ship.SIZE)));
        countOfAttacks = 0;
        scanner = new Scanner(System.in);
    }
    
    /**
     * Метод начинает игру:
     * Запускает цикл для ввода координат, по которым будет производиться атака
     */
    public void start() {
        while(ship.isAlive()) {
            int x = readCoordinate();
            
            if(ship.attack(x)) {
                System.out.print("Попал! ");
                int whole = ship.getWholePartsCount();
                if(whole > 0)
                    System.out.printf("Корабль остался цел на %d/%d!\n", whole, Ship.SIZE);
                else
                    System.out.printf("Корабль уничтожен!\n");
            } else {
                System.out.println("Мимо!");
            }
            
            countOfAttacks++;
        }
        
        scanner.close();
        
        if(countOfAttacks < 6)
            System.out.println(RESULT_PIRATE_MESSAGE);
        else if(countOfAttacks < 9)
            System.out.println(RESULT_GOOD_MESSAGE);
        else
            System.out.println(RESULT_EGOR_MESSAGE);
        
        System.out.printf("Игра окончена. Кол-во атак: %d", countOfAttacks);
    }
    
    /**
     * Возвращает координату для атаки, полученную от пользователя
     * Не допускает ввода отрицательных чисел и чисел, больших чем LENGTH_OF_CHANNEL
     * 
     * @return координата для атаки, полученная от пользователя
     */
    private int readCoordinate() {
        System.out.print(ENTER_COORDINATE_MESSAGE);
        int c = scanner.nextInt();
        
        while(c < 0 || c >= LENGTH_OF_CHANNEL) {
            System.out.printf(INCORRECT_COORDINATE_FORMAT, LENGTH_OF_CHANNEL);
            c = scanner.nextInt();
        }
        
        return c;
    }
}
